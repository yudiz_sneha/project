//   Sidebar

  var container = document.querySelector(".container");
  container.addEventListener("click", function(){
		document.querySelector("body").classList.toggle("active");
	})

    // Date and Time

function display_c(){
    var refresh=1000; // Refresh rate in milli seconds
    mytime=setTimeout('display_dt()',refresh)
  }
  
  function display_dt() {
   var CDate = new Date()
   var NewDate=CDate.toDateString(); 
   NewDate = NewDate + " - " + CDate.toLocaleTimeString();
   document.getElementById('dt').innerHTML = NewDate;
   display_c();
  }

//   Tasks

const form = document.querySelector('form');
	let list = document.querySelector('.list');
	let id = 1;
	if(JSON.parse(localStorage.getItem("todoItems")) === null || JSON.parse(localStorage.getItem("todoItems")).length == 0){
		id = 1;
	}else{
		id = JSON.parse(localStorage.getItem("todoItems"))[JSON.parse(localStorage.getItem("todoItems")).length -1].id+1 ;
		JSON.parse(localStorage.getItem("todoItems")).map(todo=>{

			let div = document.createElement('div');
			div.setAttribute("class","listItem");
			let input = document.createElement('input');
			input.setAttribute("type","checkbox");
			input.setAttribute("id",todo.id);
			div.append(input);
			let h1 = document.createElement('h1');
			h1.setAttribute("class","itemHeading");
			h1.innerHTML = todo.todo;
			div.append(h1);
			list.append(div);
		});
	}
	const inputs = document.querySelectorAll('input[type="checkbox"]');
	let nonDItems = [];
	for(let i =0; i<inputs.length; i++){
		inputs[i].addEventListener("click",e=>{
			JSON.parse(localStorage.getItem("todoItems")).map(todo=>{
				if(todo.id != e.target.id){
					nonDItems.push(todo);
				}
				localStorage.setItem("todoItems",JSON.stringify(nonDItems));
			});
				window.location.reload();
				alert('Well Done!');
		});
	}

	//form submit
	form.onsubmit = e=>{
		e.preventDefault();
		const value = e.target.name.value;
		if(value == ''){
			alert('Please Enter Something');
		}else{
			let items = [];
			let item = {
						id:id,
						todo:value
					};
			if(JSON.parse(localStorage.getItem("todoItems")) === null){
				items.push(item);
				localStorage.setItem('todoItems',JSON.stringify(items));
				window.location.reload();
				alert('Successfully Added!');
			}else{
				JSON.parse(localStorage.getItem("todoItems")).map(todo=>{
					items.push(todo);
				});
				items.push(item);
				localStorage.setItem('todoItems',JSON.stringify(items));
				window.location.reload();
				alert('Successfully Added!');
			}
		}
	}

// Registration

window.onload = function() {
};

function register() { 
    var email = document.getElementsByName("email")[0].value;
    
    let userDetails = JSON.parse(window.localStorage.getItem('users'));

    if(userDetails){ 
        var isUserExists = userDetails.filter(v=>v.email == email);
        if(isUserExists.length){ 
            alert("Your account is already registered...");
            return false;
        }    
    }
    if(validation() == true){                 
        var first_name = document.getElementsByName("first_name")[0].value;
        var last_name = document.getElementById("last_name").value;
        var mobile = document.getElementById("mobile").value;
        var dob =  document.getElementById("dob").value; 
        var email = document.getElementsByName("email")[0].value;
        var password1 = document.getElementById("password1").value;
        var password2 = document.getElementById("cpassword").value;
        var gender = document.querySelector('input[name = "gender"]:checked').value; 

        const obj  = {first_name:first_name,dob:dob,last_name:last_name,email:email,mobile:mobile,password:password1,gender:gender,isLoggedIn:false};

        userDetails = (userDetails)?userDetails:[];
        userDetails.push(obj);
        window.localStorage.setItem('users',JSON.stringify(userDetails));
        location.href="login.html";
    }            
    
}

function validate() {    
	var fname = document.reg_form.fname;    
	var lname = document.reg_form.lname;      
	var gender = document.reg_form.gender;    
	var email = document.reg_form.email;    
	var mobile = document.reg_form.mobile;
	var dob = document.reg_form.dob;
	var password = document.getElementById("password1").value;
	var confirmPassword = document.getElementById("cpassword").value;

	if (fname.value.length <= 0) {    
		alert("Firstname is required");    
		fname.focus();    
		return false;    
	}    
	if (lname.value.length <= 0) {    
		alert("Lastname is required");    
		lname.focus();    
		return false;    
	}       
	if (gender.value.length <= 0) {    
		alert("Gender is required");    
		gender.focus();    
		return false;    
	}    
	if (email.value.length <= 0) {    
		alert("Email ID is required");    
		email.focus();    
		return false;    
	}    
	if (mobile.value.length <= 0) {    
		alert("Mobile number is required");    
		mobile.focus();    
		return false;    
	}   
	if (dob.value.length <= 0) {    
		alert("DOB is required");    
		dob.focus();    
		return false;    
	}     

	if (password != confirmPassword) {
		alert("Passwords do not match");
	
	}
	return false;
}
// Login

function login(){
    if(validation() == true){
        var email = document.getElementsByName("email")[0].value;
        var password  = document.getElementsByName("password")[0].value;

        const userDetails = JSON.parse( window.localStorage.getItem('users') );

        if(userDetails){ 
            var isUserExists = userDetails.filter(v => v.email == email && v.password == password);
                isUserExists[0].isLoggedIn = true;
            if(isUserExists.length){                        
                
                window.localStorage.setItem('token',JSON.stringify(isUserExists[0]));
                alert("Successfully Logged In");
                location.href= "home.html"
            }else{
                alert("Incorrect Credentials")
            }
        }else{
            alert("Incorrect Credentials")
         }

        

        
    }        
}   
function validation() { 
    var email = document.getElementsByName("email")[0].value;
    var password  =document.getElementsByName("password")[0].value; 
    if(email==""){ 
        document.getElementById("email_error").innerHTML = "Email ID is required";
        return false;
    }
    if(password==""){ 
        document.getElementById("password_error").innerHTML = "Password is required";
        return false;
    }
    return true;
}
function checkvalue(id){ 
    document.getElementById(id).innerHTML = "";
}

// Weather Report

let lon;
let lat;
let temperature = document.querySelector(".temp");
let summary = document.querySelector(".summary");
let loc = document.querySelector(".location");
let icon = document.querySelector(".icon");
const kelvin = 273;
  
window.addEventListener("load", () => {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition((position) => {
      console.log(position);
      lon = position.coords.longitude;
      lat = position.coords.latitude;
  
      // API ID
      const api = "6d055e39ee237af35ca066f35474e9df";
  
      // API URL
      const base =
`http://api.openweathermap.org/data/2.5/weather?lat=${lat}&` +
`lon=${lon}&appid=6d055e39ee237af35ca066f35474e9df`;
  
      // Calling the API
      fetch(base)
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          console.log(data);
          temperature.textContent = 
              Math.floor(data.main.temp - kelvin) + "°C";
          summary.textContent = data.weather[0].description;
          loc.textContent = data.name + "," + data.sys.country;
          let icon1 = data.weather[0].icon;
          icon.innerHTML = 
              `<img src="icons/${icon1}.svg" style= 'height:10rem'/>`;
        });
    });
  }
});

// Change Password

function chgpass() {
	var oldpasswprd = document.getElementById('oldPassword').value;
	var newpassword = document.getElementById('newPassword').value;
	var confirmpassword = document.getElementById('confirmPassword').value;
	if (oldPassword == "" || newpassword == "" || confirmpassword == "") {
		alert('Please fill all the details');
	}
	else if (oldpasswprd == newpassword) {
		alert("Old password and New Password cannot be same");
	}
	else if (newpassword != confirmpassword) {
		alert("password mismatch");
	}
}

// Logout

function clearData() {
	window.localStorage.clear();
	this.showData();
}




